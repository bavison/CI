workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH

stages:
  - snapshot_fetch
  - snapshot_export_hdrs
  - snapshot_export_libs
  - snapshot_package
  - latest_fetch
  - latest_export_hdrs
  - latest_export_libs
  - latest_package
  - cleanup

# For most jobs, we don't want the Runner to do any git operations
variables:
  GIT_STRATEGY: none

# Other default settings
default:
  before_script:
    - source RiscOS/Env/ROOL/OMAP5.sh

snapshot_fetch:
  stage: snapshot_fetch
  tags: [ cross ]
  variables:
    GIT_STRATEGY: fetch
  before_script:
    # Runner only cleans superproject by default - explicitly clean submodules
    - git submodule foreach 'git clean -xdf && git checkout -f'
    - git submodule foreach 'git fetch origin'
    - git submodule update --init --jobs 8
    # Tweaks are required to several components to permit cross-compilation, but are currently stuck in review.
    # Pull them into the source tree explicitly (note that this assumes they are all in bavison's fork projects, which is true at time of writing).
    # Eventually we should be able to remove this line entirely, which will speed up builds a lot.
    # Build a "Manifest" file at the top level while we do this.
    - rm -f Manifest Pending Diverged
    - >
        git submodule foreach '
        oldrev=$(git rev-parse HEAD);
        git remote remove bavison || :;
        git remote add -f bavison https://gitlab.riscosopen.org/bavison/$(basename $(pwd)).git;
        git branch -D CrossCompilationSupport || :;
        ( git checkout CrossCompilationSupport &&
        ( echo "$sm_path" >> $toplevel/Pending;
        [ $oldrev = $(git merge-base $oldrev HEAD) ] ||
        echo "$sm_path" >> $toplevel/Diverged ) ) || :;
        ( echo -n "$sm_path: ";
        git describe --abbrev=8 --tags --always ) >> $toplevel/Manifest;'
    - echo Using snapshot build tree generated with this pipeline > BuildInfo
    - echo $CI_PIPELINE_URL >> BuildInfo
    - echo Search for \'cat Manifest\' in $CI_JOB_NAME job log for versions >> BuildInfo
    - echo $CI_JOB_URL >> BuildInfo
    - '# Fetched the following versions of each submodule.'
    - cat Manifest
    - '# Using CrossCompilation branch for the following submodules.'
    - '# If you rely on any of them in your builds, remember that they are subject'
    - '# to change during review, so consider marking your submission as dependent'
    - '# upon the relevant MR.'
    - cat Pending
    - '# For the following submodules, mainline development has diverged from the'
    - '# CrossCompilationSupport branch. If you require features from mainline since'
    - '# this point, nag Ben to rebase the CrossCompilationSupport branch.'
    - cat Diverged
  script: echo
  dependencies: []

snapshot_export_hdrs:
  stage: snapshot_export_hdrs
  tags: [ cross ]
  script: srcbuild export_hdrs
  dependencies: []
  # For now, we expect some failed components
  allow_failure: true

snapshot_export_libs:
  stage: snapshot_export_libs
  tags: [ cross ]
  script: srcbuild export_libs
  dependencies: []
  # For now, we expect some failed components
  allow_failure: true

snapshot_package_rom:
  stage: snapshot_package
  tags: [ cross ]
  before_script: []
  script: echo
  dependencies: []
  artifacts:
    paths:
      - RiscOS/Images/a*

latest_fetch:
  stage: latest_fetch
  tags: [ cross ]
  variables:
    GIT_STRATEGY: fetch
  before_script:
    # Runner only cleans superproject by default - explicitly clean submodules
    - git submodule foreach 'git clean -xdf && git checkout -f'
    - git submodule update
    - git submodule update --remote --no-fetch
    # Tweaks are required to several components to permit cross-compilation, but are currently stuck in review.
    # Pull them into the source tree explicitly (note that this assumes they are all in bavison's fork projects, which is true at time of writing).
    # Eventually we should be able to remove this line entirely, which will speed up builds a lot.
    # Build a "Manifest" file at the top level while we do this.
    - rm -f Manifest Pending Diverged
    - >
        git submodule foreach '
        oldrev=$(git rev-parse HEAD);
        git remote remove bavison || :;
        git remote add -f bavison https://gitlab.riscosopen.org/bavison/$(basename $(pwd)).git;
        git branch -D CrossCompilationSupport || :;
        ( git checkout CrossCompilationSupport &&
        ( echo "$sm_path" >> $toplevel/Pending;
        [ $oldrev = $(git merge-base $oldrev HEAD) ] ||
        echo "$sm_path" >> $toplevel/Diverged ) ) || :;
        ( echo -n "$sm_path: ";
        git describe --abbrev=8 --tags --always ) >> $toplevel/Manifest;'
    - echo Using latest build tree generated with this pipeline > BuildInfo
    - echo $CI_PIPELINE_URL >> BuildInfo
    - echo Search for \'cat Manifest\' in $CI_JOB_NAME job log for versions >> BuildInfo
    - echo $CI_JOB_URL >> BuildInfo
    - '# Fetched the following versions of each submodule.'
    - cat Manifest
    - '# Using CrossCompilation branch for the following submodules.'
    - '# If you rely on any of them in your builds, remember that they are subject'
    - '# to change during review, so consider marking your submission as dependent'
    - '# upon the relevant MR.'
    - cat Pending
    - '# For the following submodules, mainline development has diverged from the'
    - '# CrossCompilationSupport branch. If you require features from mainline since'
    - '# this point, nag Ben to rebase the CrossCompilationSupport branch.'
    - cat Diverged
  script: echo
  dependencies: []

latest_export_hdrs:
  stage: latest_export_hdrs
  tags: [ cross ]
  script: srcbuild export_hdrs
  dependencies: []
  # For now, we expect some failed components
  allow_failure: true

latest_export_libs:
  stage: latest_export_libs
  tags: [ cross ]
  script: srcbuild export_libs
  dependencies: []
  # For now, we expect some failed components
  allow_failure: true

latest_package_rom:
  stage: latest_package
  tags: [ cross ]
  before_script: []
  script: echo
  dependencies: []
  artifacts:
    paths:
      - RiscOS/Images/a*

latest_package_tree:
  stage: latest_package
  tags: [ cross ]
  before_script: []
  script: echo
  dependencies: []
  artifacts:
    paths:
      - BuildInfo
      - RiscOS/*

cleanup:
  stage: cleanup
  tags: [ cross ]
  before_script: []
  script:
  - git submodule foreach 'git clean -xdf'
  dependencies: []
