workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH

stages:
  - static_analysis
  - softload
  - disc
  - deploy
  - cleanup

gitattributes:
  stage: static_analysis
  tags: [ cross ]
  rules:
    - if: '$SUPPRESS_JOBS !~ /\bgitattributes\b/'
  script:
    - >
        function top_dir {
        if [ -d "$1" ] && ! grep -q "^$1/\*\* " .gitattributes; then
        echo ".gitattributes lacks pattern for top-level $1 directory"; false;
        fi
        };
        function nested_dir {
        if [ $(find . -mindepth 2 -type d -name "$1" | wc -c) -ne 0 ] &&
        ! grep -q "^\*\*/$1/\*\* " .gitattributes; then
        echo ".gitattributes lacks pattern for nested $1 directory"; false;
        fi
        };
        function dir_type {
        top_dir "$1";
        nested_dir "$1";
        };
        function file_type {
        if [ $(find . -type f -name "*,$1" | wc -c) -ne 0 ] &&
        ! grep -q "^\*,$1 " .gitattributes; then
        echo ".gitattributes lacks pattern for filetype $1"; false;
        fi
        };
        dir_type  Hdr;
        dir_type  hdr;
        dir_type  s;
        dir_type  awk;
        dir_type  bas;
        file_type ffb;
        file_type fd1;
        dir_type  c;
        dir_type  h;
        dir_type  x;
        dir_type  CMHG;
        dir_type  cmhg;
        dir_type  c++;
        file_type fe1;
        dir_type  pl;
        file_type 102;
  allow_failure: true

gitignore:
  stage: static_analysis
  tags: [ cross ]
  rules:
    - if: '$SUPPRESS_JOBS !~ /\bgitignore\b/'
  script:
    - test -f .gitignore
  allow_failure: true

license:
  stage: static_analysis
  tags: [ cross ]
  rules:
    - if: '$SUPPRESS_JOBS !~ /\blicense\b/'
  script:
    - >
        test -f LICENSE ||
        test -f License.txt ||
        test -f LICENCE ||
        test -f Licence ||
        test -f COPYING ||
        test -f Copying ||
        test -f COPYINGLIB ||
        test -f COPYING.LESSER ||
        test -f Artistic
  allow_failure: true

versionnum:
  stage: static_analysis
  tags: [ cross ]
  rules:
    - if: '$SUPPRESS_JOBS !~ /\bversionnum\b/'
  script:
    - test -f VersionNum
  allow_failure: true

head_log:
  stage: static_analysis
  tags: [ cross ]
  rules:
    - if: '$SUPPRESS_JOBS !~ /\bhead_log\b/'
  script:
    - >
        git log -1 --pretty=format:%B | awk '
        NR==1 && length($0)>70 { print "Commit log summary line exceeds 70 characters"; result=1 }
        NR==2 && length($0)>0  { print "Commit log requires a single-line summary followed by a blank line"; result=1 }
        NR>=2 && length($0)>80 { print "Commit log line "NR" exceeds 80 characters"; result=1 }
        END { exit result }'
  allow_failure: true

merge_log:
  stage: static_analysis
  tags: [ cross ]
  rules:
    - if: '$CI_MERGE_REQUEST_ID && $SUPPRESS_JOBS !~ /\bmerge_log\b/'
  script:
    - >
        git remote show | grep target > /dev/null && git remote remove target || true;
        git remote add -t $CI_MERGE_REQUEST_TARGET_BRANCH_NAME -f target $CI_MERGE_REQUEST_PROJECT_URL.git;
        has_versionnum=1; custom_versionnum=0;
        if test -f VersionNum; then grep -q "This file is automatically maintained by srccommit, do not edit manually." VersionNum || custom_versionnum=1; else has_versionnum=0; fi;
        h=$(git rev-parse HEAD);
        for r in $(git rev-list --reverse $(git merge-base HEAD target/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME)..HEAD);
        do
        git log -1 --pretty=format:%B $r | awk '
        BEGIN { keywords=0; nochange=0; notag=0; tag=""; printf("\033[91m") }
        /!NoChange|!NoTag|!Tag\([^)]+\)/ { ++keywords }
        /!NoChange/ { if ("'$r'"=="'$h'") nochange=1; else { print "Commit '$r' is not tip of merge request and uses !NoChange keyword in log"; result=1 } }
        /!NoTag/ { if ("'$r'"=="'$h'") notag=1; else { print "Commit '$r' is not tip of merge request and uses !NoTag keyword in log"; result=1 } }
        /!Tag\([^)]+\)/ { if ("'$r'"=="'$h'") tag=gensub(/^.*!Tag\(([^)]+)\).*$/, "\\1", "g");
        else { print "Commit '$r' is not tip of merge request and uses !Tag keyword in log"; result=1 } }
        NR==1 && length($0)>70 { print "Commit '$r' log summary line exceeds 70 characters"; result=1 }
        NR==2 && length($0)>0  { print "Commit '$r' log requires a single-line summary followed by a blank line"; result=1 }
        NR>=2 && length($0)>80 { print "Commit '$r' log line "NR" exceeds 80 characters"; result=1 }
        END {
        if (keywords > 1) { print "Commit '$r' uses multiple keywords in log"; result = 1 }
        if ("'$r'"=="'$h'") {
        if (!'$has_versionnum' && tag=="") { print "Either create a VersionNum file or use !Tag keyword in log"; result=1 }
        if ('$custom_versionnum' && tag=="" && notag==0) { print "Non-standard VersionNum file requires either !Tag or !NoTag keyword in log"; result=1 }
        if (tag!="") {
        if ('$has_versionnum' && !'$custom_versionnum') { print "!Tag keyword must not be used with standard VersionNum file"; result=1 }
        if (system("git check-ref-format \"tags/" tag "\"")) { print "Invalid tag name"; result = 1 }
        else if (!system("git ls-remote --exit-code --tags target " tag " > /dev/null")) { print "Tag already exists in destination repository"; result=1 }
        }
        printf("\033[39m")
        }
        exit result;
        }';
        done
  allow_failure: true

makefile:
  stage: static_analysis
  tags: [ cross ]
  rules:
    - if: '$SUPPRESS_JOBS !~ /\bmakefile\b/'
  script:
    - >
        for f in $(find *
        -name "makefile" -o
        -name "*Make*" -o
        -name "*.mk" -o
        -name "*,fe1" -o
        -name "AutoGenMfS"); do awk '
        ''STATE==2                    { print "'$f' contains unstripped dynamic dependencies"; exit 1 }
        ''STATE==0 && /^ /            { print "'$f' line "NR" should start with a tab character" }
        ''                            { STATE=0 }
        ''/\\$/                       { STATE=1 }
        ''/^# Dynamic dependencies:$/ { STATE=2 }
        ' $f; done
  allow_failure: true

head_whitesp:
  stage: static_analysis
  tags: [ cross ]
  rules:
    - if: '$SUPPRESS_JOBS !~ /\bhead_whitesp\b/'
  script:
    - >
        status=0;
        exceptions=();
        while read -d " " part; do
        if [ "$part" != "" ]; then
        exceptions=("${exceptions[@]}" -path "$part" -prune -o);
        fi;
        done <<< "$WHITESPACE_WHITELIST ";
        find . "${exceptions[@]}"
        -path "./.git" -prune -o
        -path "./Resources*/Messages" -prune -o
        -type f -print |
        xargs grep -In $'\(^  *\t\|[\t ]$\)'
        || status=1; test $status != 0
  allow_failure: true

merge_whitesp:
  stage: static_analysis
  tags: [ cross ]
  rules:
    - if: '$CI_MERGE_REQUEST_ID && $SUPPRESS_JOBS !~ /\bmerge_whitesp\b/'
  script:
    - >
        git remote show | grep target > /dev/null && git remote remove target || true;
        git remote add -t $CI_MERGE_REQUEST_TARGET_BRANCH_NAME -f target $CI_MERGE_REQUEST_PROJECT_URL.git;
        awk '
        ''BEGIN {
        ''  result = 0;
        ''  cmd = "git diff -b -U0 target/'$CI_MERGE_REQUEST_TARGET_BRANCH_NAME' HEAD";
        ''  while ((cmd | getline) > 0) {
        ''    if ($1 == "+++")
        ''      file = gensub(/^b\//, "", "1", $2);
        ''    else if ($1 == "@@")
        ''      line = gensub(/+([0-9]+).*/, "\\1", "1", $3);
        ''    else if ($0 ~ /^+/) {
        ''      for (nearline = line-20; nearline <= line+20; ++nearline)
        ''        change[file][nearline];
        ''      ++line;
        ''    }
        ''  }
        ''  close(cmd);
        ''  cmd = "git diff -U0 target/'$CI_MERGE_REQUEST_TARGET_BRANCH_NAME' HEAD";
        ''  while ((cmd | getline) > 0) {
        ''    if ($1 == "+++") {
        ''      file = gensub(/^b\//, "", "1", $2);
        ''      change[file]["dummy"]
        ''    } else if ($1 == "@@") {
        ''      line = gensub(/+([0-9]+).*/, "\\1", "1", $3);
        ''      had_error = 0;
        ''    } else if ($0 ~ /^-/) {
        ''      if ($0 ~ /( +\t|[\t ]$)/)
        ''        had_error = 1;
        ''    } else if ($0 ~ /^+/) {
        ''      if (had_error && !(line in change[file])) {
        ''        print file " line " line " probably removes whitespace error";
        ''        ++result;
        ''      }
        ''      ++line;
        ''    }
        ''  }
        ''  close(cmd)
        ''}
        ''END { exit result >= 10 }'
  allow_failure: true

copyright:
  stage: static_analysis
  tags: [ cross ]
  rules:
    - if: '$SUPPRESS_JOBS !~ /\bcopyright\b/'
  script:
    - >
        status=0;
        exceptions=();
        while read -d " " part; do
        if [ "$part" != "" ]; then
        exceptions=("${exceptions[@]}" -path "$part" -prune -o);
        fi;
        done <<< "$COPYRIGHT_WHITELIST ./.git ./Resources ";
        for f in $(find . "${exceptions[@]}" -type f -print |
        grep -E '(/(hdr|s|awk|bas|c|h|x|cmhg|c\+\+|pl)/|,ffb$|,fd1$|,102$)'); do
        grep -q -w -E '(Copyright|SPDX-FileCopyrightText)' $f || echo $f; done |
        tee >( wc -l ) 1>&2 | { read lines; exit $lines; }
  allow_failure: true

cppcheck:
  stage: static_analysis
  tags: [ cross ]
  rules:
    - if: '$SUPPRESS_JOBS !~ /\bcppcheck\b/'
  script:
    - 'curl --location --output ~/cache/common/Disc.zip -z ~/cache/common/Disc.zip "https://gitlab.riscosopen.org/Products/Disc/-/jobs/artifacts/master/download?job=latest_package_tree"'
    - unzip -q ~/cache/common/Disc.zip
    - source RiscOS/Env/ROOL/Disc.sh
    # Build the module in case we require that one or more autogenerated header files exist
    - COMPONENT=Config2PluginPinSetup TARGET=PinSetup INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Config2PluginPinSetup TARGET=PinSetup INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Config2PluginPinSetup TARGET=PinSetup INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Config2PluginPinSetup TARGET=PinSetup INSTDIR=$INSTALLDIR make -k standalone  || true
    - CFLAGS=$( { cd objs && { echo $'cflags:\n\t@echo ${CINCLUDES} ${CDEFINES}'; cat ../Makefile; } | make -f -; } || true )
    - test ! -d c || { { { { cppcheck --enable=all --suppress=shiftNegativeLHS --suppress=purgedConfiguration --suppress=unusedFunction --suppress=variableScope --inline-suppr $CPPCHECK_EXTRA --std=c99 $CFLAGS -UNAMESPACE_OSLIB -U__swi --verbose --max-configs=100 objs/*.c; } 3>&1 1>&2 2>&3; } | tee >( grep -v '^[^ ]* information[:] ' | grep -v '^$' | wc -l >&3 ); } 3>&1 1>&2; } | { read lines; exit $lines; }
  allow_failure: true

softload:
  stage: softload
  tags: [ cross ]
  rules:
    - if: '$SUPPRESS_JOBS !~ /\bsoftload\b/'
  script:
    - 'curl --location --output ~/cache/common/Disc.zip -z ~/cache/common/Disc.zip "https://gitlab.riscosopen.org/Products/Disc/-/jobs/artifacts/master/download?job=latest_package_tree"'
    - unzip -q ~/cache/common/Disc.zip && rm -rf RiscOS/Install
    - '[ ! -e BuildInfo ] || cat BuildInfo'
    - source RiscOS/Env/ROOL/Disc.sh
    - COMPONENT=Config2PluginPinSetup TARGET=PinSetup INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Config2PluginPinSetup TARGET=PinSetup INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Config2PluginPinSetup TARGET=PinSetup INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Config2PluginPinSetup TARGET=PinSetup INSTDIR=$INSTALLDIR make -k install
  artifacts:
    paths:
      - RiscOS/Install/*
  # For now, we expect some failed components
  allow_failure: true

softload_gnu:
  stage: softload
  tags: [ cross ]
  rules:
    - if: '$ENABLE_JOBS =~ /\bsoftload_gnu\b/'
  variables:
    TOOLCHAIN: GNU
  script:
    - 'curl --location --output ~/cache/common/Disc.zip -z ~/cache/common/Disc.zip "https://gitlab.riscosopen.org/Products/Disc/-/jobs/artifacts/master/download?job=latest_package_tree"'
    - unzip -q ~/cache/common/Disc.zip && rm -rf RiscOS/Install
    - '[ ! -e BuildInfo ] || cat BuildInfo'
    - source RiscOS/Env/ROOL/Disc.sh
    - COMPONENT=Config2PluginPinSetup TARGET=PinSetup INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Config2PluginPinSetup TARGET=PinSetup INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Config2PluginPinSetup TARGET=PinSetup INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Config2PluginPinSetup TARGET=PinSetup INSTDIR=$INSTALLDIR make -k install
  artifacts:
    paths:
      - RiscOS/Install/*
  # For now, we expect some failed components
  allow_failure: true

cleanup:
  stage: cleanup
  tags: [ cross ]
  script:
  - git clean -xdf
